package com.company.hw3;

import java.util.Arrays;

public class EqualPermutations {
    public boolean ePermutations(String str1, String str2) {
        if (str1.length() != str2.length()){
            return false;
        }
        char[] result1 = str1.toCharArray();
        char[] result2 = str2.toCharArray();
        Arrays.sort(result1);
        Arrays.sort(result2);
        String nStr1 = new String(result1);
        String nStr2 = new String(result2);
        return nStr1.equals(nStr2);
    }

}
