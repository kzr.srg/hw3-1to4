package com.company.hw3;

public class Main {
    public static void main(String[] args) {
       //1.
        System.out.println("================== 1 ===================");
        Person person = new Person("Иванов Иван Иванович");
        System.out.println(person);

        Person person1 = new Person("Иванов","Иван","Иванович");
        System.out.println(person);
        //Result:
        //patronymic='Иванов', name='Иван', surname='Иванович'
        //patronymic='Иванов', name='Иван', surname='Иванович'

        //2.
        System.out.println("================== 2 ===================");
        IntArray intArray = new IntArray();
        System.out.println(intArray);
        intArray.sort();
        System.out.println(intArray);
        //Result:
        //1 3 5 -10 3 7 -1 -3 4
        //1 1 9 9 9 16 25 49 100

        //3.
        System.out.println("================== 3 ===================");
       AllPermutations allPermutations = new AllPermutations();
       char test[]={'a','b','c'};
       allPermutations.show(test);
       //Result:
       // abc,acb,bac,bca,cba,cab

        //4.
        System.out.println("================== 4 ===================");
        EqualPermutations equalPermutations = new EqualPermutations();
        String str1 = "asdfg";
        String str2 = "gdsa";
        System.out.println(equalPermutations.ePermutations(str1,str2));
        //Result:
        //false

        String str3 = "asdfg";
        String str4 = "gfdsa";
        System.out.println(equalPermutations.ePermutations(str3,str4));
        //Result:
        //true

        System.out.println("================== End ==================");
    }
}
