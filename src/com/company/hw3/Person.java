package com.company.hw3;

public class Person {
    private String patronymic;
    private String name;
    private String surname;

    @Override
    public String toString() {
        return  "patronymic='" + patronymic + '\'' +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'';
    }

    public Person(String patronymic, String name, String surname) {
        this.patronymic = patronymic;
        this.name = name;
        this.surname = surname;
    }

    public Person(String fullName) {
        String[] fio = fullName.split(" ");
        for (int i = 0; i < fio.length; i++) {
            switch (i) {
                case 0: {
                    patronymic = fio[i];
                    break;
                }
                case 1: {
                    name = fio[i];
                    break;
                }
                case 2: {
                    surname = fio[i];
                    break;
                }
            }
        }
    }
}
