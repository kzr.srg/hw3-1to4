package com.company.hw3;


import java.util.Arrays;

public class IntArray {
    int[] array = {1, 3, 5, -10, 3, 7, -1, -3, 4};

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < array.length; i++) {
            stringBuilder.append(array[i] + " ");
        }
        return stringBuilder.toString();
    }

    public void sort() {
        for (int i = 0; i < array.length; i++) {
            array[i] = array[i] * array[i];
        }
        boolean item = true;
        while (item) {
            item = false;
            for (int i = 1; i < array.length; i++) {
                if (array[i] < array[i - 1]) {
                    array[i - 1] = array[i] + array[i - 1];
                    array[i] = array[i - 1] - array[i];
                    array[i - 1] = array[i - 1] - array[i];
                    item = true;
                }
            }
        }
    }
}
