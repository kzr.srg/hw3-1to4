package com.company.hw3;

public class AllPermutations {
    StringBuilder stringBuilder = new StringBuilder();

    public  void runPermutation(char str[], int start) {
        if (start < str.length) {
            for (int i = start; i < str.length; i++) {
                char tmp[] = new char[str.length];

                for (int j = 0; j < str.length; j++)
                    tmp[j] = str[j];
                char charStart = tmp[start];

                tmp[start] = tmp[i];
                tmp[i] = charStart;

                if (start == str.length - 1) {
                    printArray(tmp);
                }
                runPermutation(tmp, start + 1);
            }
        }
    }

    public  void printArray(char chars[]) {
        StringBuffer str = new StringBuffer();
        for (int i = 0; i < chars.length; i++) {
            str.append(chars[i]);
        }
        stringBuilder.append(str.toString() + ",");
    }

    public  void show(char[] strtest) {
        runPermutation(strtest, 0);
        stringBuilder.delete(stringBuilder.length() - 1, stringBuilder.length());
        System.out.println(stringBuilder.toString());
    }

}
